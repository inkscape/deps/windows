# Windows build-time dependencies

This repository collects the [msys2](https://www.msys2.org)-based dependencies to build Inkscape on Windows. The heavy lifting is done by [`msys2installdeps.sh`](https://gitlab.com/inkscape/inkscape/-/blob/master/buildtools/msys2installdeps.sh) combined with minor additions for testing and packaging (see [`.gitlab-ci.yml`](.gitlab-ci.yml) for  details). The goal is to have a self-contained msys2 installation folder (here: `C:/inkN`, with `N` being the `CI_PIPELINE_IID`) so we can archive it and use it in CI.

Releases are created manually by tagging a commit using the pattern `rN`, with `N` being the corresponding `CI_PIPELINE_IID` from above. Inkscape's CI can then be updated to use a newer release.

While this approach creates an additional maintenance burden as updates to `msys2installdeps.sh` in Inkscape's repository no longer affect CI immediately, we gain the ability to build Inkscape against a fixed set of dependenies instead of a moving target (msys2 follows the rolling release model and does not provide an archive of old versions), and reproducible builds are of much higher value to us.

## license

[GPL-2.0-or-later](LICENSE)

### attributions

Repository logo taken from [Microsoft icons created by Pixel perfect - Flaticon](https://www.flaticon.com/free-icons/microsoft").
